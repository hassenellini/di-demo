package com.phoenix.didemo.service.impl;

import com.phoenix.didemo.service.GreetingRepository;
import org.springframework.stereotype.Component;

@Component
public class GreetingRepositoryImpl implements GreetingRepository {
    @Override
    public String getEnglishGreeting() {
        return "hello";
    }

    @Override
    public String getSpanishGreeting() {
        return "hola";
    }

    @Override
    public String getFrenchGreeting() {
        return "salut";
    }
}
