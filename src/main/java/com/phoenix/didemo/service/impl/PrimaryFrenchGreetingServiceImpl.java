package com.phoenix.didemo.service.impl;

import com.phoenix.didemo.service.GreetingRepository;
import com.phoenix.didemo.service.GreetingService;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Service
@Primary
@Profile("fr")
public class PrimaryFrenchGreetingServiceImpl implements GreetingService {

    private GreetingRepository greetingRepository;

    @Override
    public String sayGreeting() {
        return this.greetingRepository.getFrenchGreeting();
    }
}
