package com.phoenix.didemo.service.impl;

import com.phoenix.didemo.service.GreetingService;
import org.springframework.stereotype.Service;

@Service
public class SetterGreetingServiceImpl implements GreetingService {
    @Override
    public String sayGreeting() {
        return "hello - I was injected by the setter";
    }
}
