package com.phoenix.didemo.service.impl;

import com.phoenix.didemo.service.GreetingService;
import org.springframework.stereotype.Service;

@Service
public class ConstructorGreetingServiceImpl implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hello - I was injected via the constructor";
    }
}
