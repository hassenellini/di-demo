package com.phoenix.didemo.service;

public interface GreetingService {
    String sayGreeting();
}
