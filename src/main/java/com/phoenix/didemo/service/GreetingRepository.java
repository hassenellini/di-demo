package com.phoenix.didemo.service;

public interface GreetingRepository {
    String getEnglishGreeting();
    String getSpanishGreeting();
    String getFrenchGreeting();
}
